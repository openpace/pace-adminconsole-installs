//===========================================================================
//
//  File Name:    Setup.rul
//
//  Description:  Blank setup main script file
//
//  Comments:     Blank setup is an empty setup project. If you want to
//				  create a new project via. step-by step instructions use the
//				  Project Assistant.
//
//===========================================================================

// Included header files ----------------------------------------------------
#include "ifx.h"
#include "..\Script Files\Tools.rul"

// Note: In order to have your InstallScript function executed as a custom
// action by the Windows Installer, it must be prototyped as an 
// entry-point function.

// The keyword export identifies MyFunction() as an entry-point function.
// The argument it accepts must be a handle to the Installer database.
    
/* export prototype MyFunction(HWND); */

//---------------------------------------------------------------------------
// OnFirstUIAfter
//
// The OnFirstUIAfter event called by the framework after the file transfer
// of the setup when the setup is running in first install mode. By default
// this event displays UI that informs the end user that the setup has been
// completed successfully.
//---------------------------------------------------------------------------
function OnFirstUIAfter()
    STRING szTitle, szMsg1, szMsg2, szOpt1, szOpt2;
    STRING ProgramDataFolderCorrected;
    STRING svFileName;
    STRING theDir;
    STRING beforeStr;
    STRING afterStr;
    NUMBER bOpt1, bOpt2;
    
    NUMBER nResult;
    STRING szProgram;
    BOOL bLicenseAccepted;
begin
	Disable(STATUSEX);

	bOpt1   = FALSE;
    bOpt2   = FALSE;
 
    theDir = TARGETDIR;
    
	//log4j.properties changes!
		
	ProgramDataFolderCorrected = FOLDER_COMMON_APPDATA;
	
	StrReplace(ProgramDataFolderCorrected,"\\","/",0);
	
	svFileName = theDir ^ "log4j.properties";
	beforeStr = "log4j.appender.rolling.File=logs/admin_console.log";
	afterStr = "log4j.appender.rolling.File=" + ProgramDataFolderCorrected + "Open Pace/Pace Administration/logs/admin_console.log";	
	AlterLines(svFileName, beforeStr, afterStr);
    
	SetObjectPermissions(FOLDER_COMMON_APPDATA + "Open Pace\\Pace Administration",IS_PERMISSIONS_TYPE_FOLDER,"","Users",GENERIC_ALL,IS_PERMISSIONS_OPTION_ALLOW_ACCESS);
    
    //szProgram = INSTALLDIR ^ "WinWrapperForPaceAC.exe";
    //LongPathToQuote (szProgram, TRUE);
    // Add the icon to the submenu; create the submenu if necessary.
    //nResult = AddFolderIcon (FOLDER_PROGRAMS ^ "Open Pace", "Pace Administration", szProgram, "", "", 0, "", REPLACE);
    //nResult = AddFolderIcon (FOLDER_DESKTOP, "Pace Administration", szProgram, "", "", 0, "", REPLACE);
    
    
    
    // Report the results.
    //if (nResult < 0) then
        //MessageBox ("AddFolderIcon failed.", SEVERE);
        //return FALSE;
    //else
        //return TRUE;
    //endif;

    if ( BATCH_INSTALL ) then
    	SdFinishReboot ( szTitle , szMsg1 , SYS_BOOTMACHINE , szMsg2 , 0 );
    else
	    SdFinish ( szTitle , szMsg1 , szMsg2 , szOpt1 , szOpt2 , bOpt1 , bOpt2 );
	endif;
end;

//---------------------------------------------------------------------------
// OnFirstUIBefore
//
// The OnFirstUIBefore event is called by the framework when the setup is
// running in first install mode. By default this event displays UI allowing
// the end user to specify installation parameters.
//---------------------------------------------------------------------------
function OnFirstUIBefore()
    NUMBER nResult, nSetupType, nvSize, nUser;
    STRING szTitle, szMsg, szQuestion, svName, svCompany, szFile;
    STRING szLicenseFile;
    
	BOOL bCustom, bIgnore1, bIgnore2, bLicenseAccepted;
begin	
    // TO DO: if you want to enable background, window title, and caption bar title                                                                   
    // SetTitle( @PRODUCT_NAME, 24, WHITE );                                        
    // SetTitle( @PRODUCT_NAME, 0, BACKGROUNDCAPTION ); 	                  
    // Enable( FULLWINDOWMODE );						   
    // Enable( BACKGROUND );							  
    // SetColor(BACKGROUND,RGB (0, 128, 128));					   

    // Added in InstallShield 15 - Show an appropriate error message if
    // -removeonly is specified and the product is not installed.
    if( REMOVEONLY ) then
        Disable( DIALOGCACHE );
		szMsg = SdLoadString( IDS_IFX_ERROR_PRODUCT_NOT_INSTALLED_UNINST );
   		SdSubstituteProductInfo( szMsg );
		MessageBox( szMsg, SEVERE );
		abort;
    endif;
    
	nSetupType = TYPICAL;	

Dlg_SdWelcome:
    szTitle = "";
    szMsg   = "";
    nResult = SdWelcome(szTitle, szMsg);
    if (nResult = BACK) goto Dlg_SdWelcome;
	
	szTitle   = "";
	svName    = "";
    svCompany = "";

Dlg_SdRegisterUser:
    szMsg = "";
    szTitle = "";
    //nResult = SdRegisterUser( szTitle, szMsg, svName, svCompany );
    //if (nResult = BACK) goto Dlg_SdWelcome;

Dlg_SdLicense2:
    //{{IS_SCRIPT_TAG(License_File_Path)
    szLicenseFile = SUPPORTDIR ^ "Elua.rtf";
    //}}IS_SCRIPT_TAG(License_File_Path)
    //{{IS_SCRIPT_TAG(Dlg_SdLicense2)
    nResult = SdLicense2Ex( "", "", "", szLicenseFile, bLicenseAccepted, TRUE );
    //}}IS_SCRIPT_TAG(Dlg_SdLicense2)
    if (nResult = BACK) then
        goto Dlg_SdWelcome;
    else
        bLicenseAccepted = TRUE;
    endif;

Dlg_SetupType:
    szTitle = "";
    szMsg   = "";
    nResult = SetupType2(szTitle, szMsg, "", nSetupType, 0);
    if (nResult = BACK) then
        goto Dlg_SdLicense2;
    else
	    nSetupType = nResult;
        if (nSetupType != CUSTOM) then
	        nvSize = 0;
	        FeatureCompareSizeRequired(MEDIA, INSTALLDIR, nvSize);
	        if (nvSize != 0) then      
            	MessageBox(szSdStr_NotEnoughSpace, WARNING);
	            goto Dlg_SetupType;
            endif;
			bCustom = FALSE;
			//goto Dlg_SQL;
		else
			bCustom = TRUE;
        endif;
    endif;    

Dlg_SdAskDestPath:    	
    nResult = SdAskDestPath(szTitle, szMsg, INSTALLDIR, 0);
    if (nResult = BACK) goto Dlg_SetupType;

Dlg_SdFeatureTree: 
    szTitle    = "";
    szMsg      = "";
    if (nSetupType = CUSTOM) then
		nResult = SdFeatureTree(szTitle, szMsg, INSTALLDIR, "", 2);
		if (nResult = BACK) goto Dlg_SdAskDestPath;  
    endif;

Dlg_SQL:
    nResult = OnSQLLogin( nResult );
    if( nResult = BACK ) then
    	if (!bCustom) then
    		goto Dlg_SetupType;    
    	else
    		goto Dlg_SdFeatureTree;
    	endif;
    endif;

Dlg_SdStartCopy:
    szTitle = "";
    szMsg   = "";
    nResult = SdStartCopy2( szTitle, szMsg );			
	
    if (nResult = BACK) then
       goto Dlg_SQL;;
    endif;

    // Added in IS 2009 - Set appropriate StatusEx static text.
    SetStatusExStaticText( SdLoadString( IDS_IFX_STATUSEX_STATICTEXT_FIRSTUI ) );

    // setup default status
    Enable(STATUSEX);
 
    return 0;
end;

