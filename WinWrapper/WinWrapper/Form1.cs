﻿#region copyright
// Copyright (c) 2017-2018 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WinWrapper
{
    public partial class Form1 : Form
    {
        public Form1(string processString, string argString)
        {
            InitializeComponent();

            try
            {
                this.Visible = false;
                this.WindowState = FormWindowState.Minimized;

                Process p = new Process();
                p.StartInfo.FileName = processString;
                p.StartInfo.WorkingDirectory = Path.GetDirectoryName(processString);
                p.StartInfo.Arguments = argString;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                p.Start();
                p.WaitForExit();
                Close();
                //Application.Exit();
            }
            catch (Exception) { }
        }
    }
}
